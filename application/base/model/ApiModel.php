<?php
/**
 * @author simonmao
 * @version 1.0.0
 * @date 2023/3/29 23:22
 * @description
 */

namespace app\base\model;

abstract class ApiModel extends BaseModel
{
    protected $connection = 'database_api';

}