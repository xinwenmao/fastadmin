<?php
/**
 * @author simonmao
 * @version 1.0.0
 * @date 2023/3/22 15:47
 * @description
 */

namespace app\base\model;

use think\Model;

abstract class BaseModel extends Model
{
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';
    protected $dateFormat = 'Y-m-d H:i:s';

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = 'deleted';

    // 新增自动完成列表
    protected $insert = [
        'id',
    ];

    // 读取类型转换
    protected $type = [
        'id' => 'string',
        'create_time' => 'datetime',
        'update_time' => 'datetime',
        'create_by' => 'string',
        'update_by' => 'string',
        'deleted' => 'timestamp',
    ];

    /**
     * Auto set id
     *
     * @param $value
     * @return string
     * @throws \Exception
     */
    protected function setIdAttr($value)
    {
        if (isset($value)) {
            return $value;
        }
        return gen_snowflake_id();
    }

}