<?php
/**
 * @author simonmao
 * @version 1.0.0
 * @date 2023/3/22 15:23
 * @description
 */

namespace app\base\controller;

use DI\ContainerBuilder;
use DI\DependencyException;
use think\Config;
use think\Controller;
use think\Request;

class BaseController extends Controller
{

    /**
     * @throws DependencyException
     */
    public function __construct(Request $request = null)
    {
        $this->initSiteConfig();
        $this->initContainer();
        parent::__construct($request);
    }

    /**
     * @throws DependencyException
     */
    private function initContainer()
    {
        $builder = new ContainerBuilder();
        $builder->useAnnotations(true);
        $builder->useAutowiring(true);
        $builder->enableCompilation(RUNTIME_PATH . '/tmp');
        $builder->writeProxiesToFile(true, RUNTIME_PATH . '/tmp/proxies');
        $builder->addDefinitions(Config::get('di'));
        $container = $builder->build();
        $container->injectOn($this);
    }

    /**
     * 构建镜像不打包该文件，每次运行系统从数据库生成。
     *
     * @return void
     */
    private function initSiteConfig()
    {
        $filename = CONF_PATH . 'extra' . DS . 'site.php';
        if (!file_exists($filename)) {
            \app\common\model\Config::refreshFile(false);
        }
    }

}