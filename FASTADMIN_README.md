# README

该项目基于 [FastAdmin](https://gitee.com/karson/fastadmin)，在基础上进行功能定制。

Current repository from: https://gitee.com/xinwenmao/fastadmin

## 背景

为了支持异构架构（FastAdmin + Java/Spring Cloud/Spring Boot/MyBatis-Plus...），实现系统/功能/支持等的一些共通性，所以衍生出该项目作为基础。

## 变更内容

1. 支持 bigint（在 [Framework](https://gitee.com/xinwenmao/framework) 支持）
2. 引入 php-di（依赖注入）

```shell
composer require php-di/php-di --with-all-dependencies
```

3. 引入 annotations（注解）

```shell
composer install doctrine/annotations
```

4. 引入 carbon（时间处理）

```shell
composer install nesbot/carbon
```

5. 引入 php-snowflake（生成雪花 ID）

```shell
composer require godruoyi/php-snowflake -vvv
```

## 安装

1. 更新 Framework

```shell
composer update topthink/framework -vvv
```

## 说明

#### 对应 MySQL 表结构

1. 由于改用为雪花 ID，所以不需要表的自增 ID 参数；
2. 以及自动插入时间、软删除。

```
CREATE TABLE `example`
(
    `id`          BIGINT UNSIGNED DEFAULT 0                 NOT NULL COMMENT 'ID' PRIMARY KEY,
    `version`     BIGINT UNSIGNED DEFAULT 0                 NOT NULL COMMENT '版本号',
    `create_time` DATETIME        DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建时间',
    `update_time` DATETIME        DEFAULT CURRENT_TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `create_by`   BIGINT UNSIGNED DEFAULT 0                 NOT NULL COMMENT '创建人 ID',
    `update_by`   BIGINT UNSIGNED DEFAULT 0                 NOT NULL COMMENT '更新人 ID',
    `deleted`     BIGINT UNSIGNED DEFAULT 0                 NOT NULL COMMENT '删除时间'
)
    COMMENT '例子';
```

## 生成代码

针对指定数据源进行生成

```shell
php think crud --db=database_api -t tablename -u 1 -f 1
```